import { Pressable, View, Text, StyleSheet } from "react-native";

export default function CustomButton({onPress, text}) {
  return (
    <>
      <View style={styles.button}>
        <Pressable onPress={onPress}>
          <Text style={styles.text}>{text}</Text>
        </Pressable>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  button: {
    borderColor: "white",
    borderWidth: 3,
    borderRadius: 10,
    elevation: 8,
    backgroundColor: "rgba(141,14,114,0.4)",
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 20,
    width: 300,
  },
  text: {
    fontSize: 20,
    color: "#fff",
    fontWeight: "300",
    alignSelf: "center",
    textTransform: "uppercase",
  }
});
