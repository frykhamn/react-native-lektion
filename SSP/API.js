export const fetchNewToken = () =>
    fetch("http://localhost:8080/players/auth/token")
        .then((response) => response.json());
