import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  Pressable,
} from "react-native";
import { firebase } from "../../firebase";

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState("");
  
  const todosRef = firebase.firestore().collection("todos");

  useEffect(() => {
    todosRef.onSnapshot((querySnapshot) => {
      const list = [];
      querySnapshot.forEach((doc) => {
        const { item } = doc.data();
        list.push({ id: doc.id, item: item });
      });
      setTodos(list);
    });
  }, []);

  const addTodo = () => {
    todosRef
    .add({ item: newTodo })
    .then((docRef) => {
      setNewTodo('');
    })
    .catch((error) => {
      console.error(error);
    });
  };

  const updateTodo = (id, newText) => {
  
  };

  const deleteTodo = (id) => {
    todosRef
      .doc(id)
      .delete()
      .then(() => {
        console.log('Todo deleted successfully:', id);
      })
      .catch((error) => {
        console.error('Error deleting todo:', error);
      });
  };
  
  

  const renderTodoItem = ({ item }) => (
    <View
      style={{ flexDirection: "row", alignItems: "center", marginVertical: 5 }}
    >
      <Text style={{ color: "black", padding: 10 }}>{item.item}</Text>
      <Pressable onPress={() => deleteTodo(item.id)}>
        <Text style={{ color: "red", fontWeight: "bold" }}>Delete</Text>
      </Pressable>
    </View>
  );

  return (
    <View style={{ flex: 1, padding: 20 }}>
      <Text style={{ fontSize: 24, fontWeight: "bold", marginBottom: 20 }}>
        Vad ska vi göra idag?
      </Text>
      <FlatList
        data={todos}
        renderItem={renderTodoItem}
        keyExtractor={(item) => item.id}
      />
      <View style={{ flexDirection: "row", marginTop: 10 }}>
        <TextInput
          style={{ flex: 1, marginRight: 5, borderWidth: 1, padding: 5 }}
          placeholder="New Todo"
          value={newTodo}
          onChangeText={setNewTodo}
        />
        <Button title="Add" onPress={addTodo} />
      </View>
    </View>
  );
};

export default TodoList;
