import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  Button,
  FlatList,
  Pressable,
} from "react-native";

import { firebase } from "../../firebase";

const Todo = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState("");

  const db = firebase.firestore().collection("todos");

  useEffect(() => {
    db.onSnapshot((q) => {
      const list = [];
      q.forEach((doc) => {
        const { item } = doc.data();
        list.push({ id: doc.id, item: item });
      });
      setTodos(list);
    });
  }, []);

  const addTodo = () => {
    db.add({ item: newTodo })
      .then((doc) => {
        console.log(doc);
        setNewTodo("");
      })
      .catch((error) => console.error(error));
  };

  const deleteTodo = (id) => {
    db.doc(id)
      .delete()
      .then(() => {
        console.log("deleted!");
      })
      .catch((error) => console.error(error));
  };

  const renderTodoItem = ({ item }) => (
    <View
      style={{ flexDirection: "row", alignItems: "center", marginVertical: 5 }}
    >
      <Text style={{ color: "black", padding: 10 }}>{item.item}</Text>
      <Button onPress={() => deleteTodo(item.id)} title="Ta bort"></Button>
    </View>
  );

  return (
    <View style={{ flex: 1, padding: 20 }}>
      <Text style={{ fontSize: 24, fontWeight: "bold", marginBottom: 20 }}>
        Vad ska vi göra idag?
      </Text>
      <FlatList
        data={todos}
        renderItem={renderTodoItem}
        keyExtractor={(item) => item.id}
      />
      <View>
        <TextInput
          placeholder="Nått att göra"
          value={newTodo}
          onChangeText={(text) => setNewTodo(text)}
          style={{ borderColor: "black", borderWidth: 2, padding: 2 }}
        />
        <Button title="Lägg till" onPress={addTodo}></Button>
      </View>
    </View>
  );
};

export default Todo;
