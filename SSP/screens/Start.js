import { TextInput, StyleSheet, ImageBackground } from "react-native";
import CustomButton from "../components/CustomButton";
import { useState } from "react";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";

import { useNavigation } from "@react-navigation/native";

export default function Start() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const auth = getAuth();
  const navigation = useNavigation();

  const handleSignUp = () => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // User creation successful
        const user = userCredential.user;
        console.log(user.email);
        // Additional logic or actions after successful user creation
      })
      .catch((error) => {
        // User creation failed
        const errorMessage = error.message;
        alert(errorMessage);
      });
  };

  const handleLogin = async () => {
    try {
      const userCredential = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      // User is logged in successfully
      console.log("User logged in:", userCredential.user);
      navigation.navigate("Todo");
    } catch (error) {
      // Handle login error
      alert(error.message);
    }
  };

  return (
    <ImageBackground
      style={styles.body}
      source={require("../../images/Background.jpg")}
    >
      <TextInput
        placeholder="E-mail"
        placeholderTextColor="white"
        textColor="white"
        value={email}
        onChangeText={(text) => setEmail(text)}
        style={styles.input}
      />
      <TextInput
        placeholder="Lösenord"
        placeholderTextColor="white"
        value={password}
        onChangeText={(text) => setPassword(text)}
        style={styles.input}
        secureTextEntry
      />
      <CustomButton onPress={handleLogin} text={"Log in"}></CustomButton>
      <CustomButton onPress={handleSignUp} text={"Reg"}></CustomButton>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  input: {
    borderColor: "white",
    color: "white",
    borderWidth: 3,
    borderRadius: 10,
    elevation: 8,
    backgroundColor: "rgba(141,14,114,0.4)",
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 20,
    width: 300,
  },
});
