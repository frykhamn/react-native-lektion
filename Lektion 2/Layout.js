import { useState } from "react";
import { Text, TextInput, View, StyleSheet, FlatList } from "react-native";
import Button from "./Button";

export default function Layout() {
  const [newText, setNewText] = useState("");
  const [list, setList] = useState([]);

  const handlePress = () => {
    setList([...list, newText]);
    setNewText("");
    console.log("Lägg till");
  };

  const handleDelete = () => {
    console.log("Delete");
  };

  return (
    <>
      <View style={styles.viewContainer}>
        <View style={styles.inuptContainer}>
          <TextInput
            style={styles.textInput}
            value={newText}
            onChangeText={(text) => setNewText(text)}
          />
          <Button
            onPress={handlePress}
            title={"Lägg till"}
            style={{ backgroundColor: "green" }}
          />
        </View>
        <Button
          onPress={handleDelete}
          title={"Delete"}
          style={{ backgroundColor: "red" }}
        />
        <Button onPress={handlePress} title={"Hej"} />

        <View style={styles.textContainer}>
          <FlatList
            data={list}
            renderItem={({ item }) => <Text>{item}</Text>}
          />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 300,
  },
  inuptContainer: {
    flexDirection: "row",
  },
  textInput: {
    borderWidth: 1,
    width: 200,
  },
  textContainer: {
    flex: 2,
  },
});
