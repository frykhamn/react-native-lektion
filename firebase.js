// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAnBcewqcjPGrSLkjr9EZPQGIuiDyje0nU",
  authDomain: "project-5e8be.firebaseapp.com",
  projectId: "project-5e8be",
  storageBucket: "project-5e8be.appspot.com",
  messagingSenderId: "1044809925338",
  appId: "1:1044809925338:web:9c76762461d8131b409be3"
};

// Initialize Firebase
if(!firebase.apps.lenght){
  firebase.initializeApp(firebaseConfig)
}


export {firebase} ;