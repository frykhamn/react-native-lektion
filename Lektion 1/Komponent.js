import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  FlatList,
} from "react-native";

export default function Komponent() {
  const [text, setText] = useState("");
  const [word, setWord] = useState(["hej", "då"]);

  const handlePress = () => {
    setWord([...word, text]);
  };

  return (
    <View style={styles.container}>
      <Text>Hallå</Text>

      <Image source={require("./assets/mountain.png")} />
      <TextInput style={styles.input} onChangeText={setText} value={text} />
      <Button title={"Knapp"} onPress={handlePress} />

      <FlatList data={word} renderItem={({ item }) => <Text>{item}</Text>} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    height: 40,
    width: "50%",
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});