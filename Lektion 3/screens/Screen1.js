import {
  Text,
  TextInput,
  View,
  StyleSheet,
  FlatList,
  Pressable,
} from "react-native";

export default function Screen1({ navigation, route }) {

  


  const onPressHandler = () => {
    navigation.navigate("Screen_2");
  };

  return (
    <>
      <View style={styles.body}>
        <Text style={styles.text}>1</Text>
        <Pressable style={styles.button} onPress={onPressHandler}>
          <Text style={{ fontSize: 20 }}>Navigera till nr 2</Text>
        </Pressable>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: "rgba(87,11,11,0.87)",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },

  text: {
    color: "white",
    fontSize: 30,
  },

  button: {
    backgroundColor: "white",
    width: 200,
    height: 50,
    padding: 10,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
  },
});
