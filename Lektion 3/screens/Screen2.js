import { Text, TextInput, View, StyleSheet, FlatList } from "react-native";


export default function Screen2() {
    return (
      <>
        <View style={styles.body}>
          <Text style={styles.text}>Skärm nr 2</Text>
        </View>
      </>
    );
  }
  

  const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: "blue",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
  },

  text: {
    color: "white",
    fontSize: 30,
  },
});
