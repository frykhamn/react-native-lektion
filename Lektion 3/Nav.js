import { Text } from "react-native";
import Screen1 from "./screens/Screen1";
import Screen2 from "./screens/Screen2";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

export default function Nav() {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name={"Screen_1"} component={Screen1}
          initialParams={{user: "Hanna", age: 45}}
        ></Stack.Screen>
          <Stack.Screen name={"Screen_2"} component={Screen2}></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}
