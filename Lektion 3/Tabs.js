import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import Screen1 from "./screens/Screen1";
import Screen2 from "./screens/Screen2";

const Tab = createBottomTabNavigator();

export default function Tabs() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Screen_1" component={Screen1} />
        <Tab.Screen name="Screen_2" component={Screen2} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
