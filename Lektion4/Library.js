import { Camera } from "expo-camera";
import { useEffect, useRef, useState } from "react";
import { Button, View, Text, Image } from "react-native";
import * as MediaLibrary from "expo-media-library";

export default function Library() {
  const [premission, setPremission] = useState();
  const [image, setImage] = useState();
  const cameraRef = useRef(null);

  useEffect(() => {
    (async () => {
      MediaLibrary.requestPermissionsAsync();
      const camerPremission = await Camera.requestCameraPermissionsAsync();
      setPremission(camerPremission.status === "granted");
    })();
  }, []);

  if (premission === false) {
    return <Text>Tillstånd nekat</Text>;
  }

  const takePhoto = async () => {
    if (cameraRef) {
      try {
        const photo = await cameraRef.current.takePictureAsync();
       setImage(photo.uri);
      } catch (err) {
        console.log(err);
      }
    }
  };

  const savePhoto = async () => {
    if (image) {
      try {
        await MediaLibrary.createAssetAsync(image);
        setImage(null);
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <>
      <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />
      <View style={{ flex: 1 }}>
        <Camera ref={cameraRef} style={{ flex: 1 }}>
          <Button title="Take photo" onPress={takePhoto}></Button>
          <Button title="Save photo" onPress={savePhoto}></Button>
        </Camera>
      </View>
    </>
  );
}
