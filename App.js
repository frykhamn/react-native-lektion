import { useState } from "react";
import { StyleSheet, View, Text } from "react-native";
import Start from "./SSP/screens/Start";
import Todo from "./SSP/screens/Todo";
import TodoList from "./SSP/screens/TodoList";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Library from "./Lektion4/Library";

const Stack = createNativeStackNavigator();

export default function App() {

  return (
  /*   <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen options={{headerShown: false}} name={"Start"} component={Start}></Stack.Screen>
        <Stack.Screen name={"Todo"} component={Todo}></Stack.Screen>
        <Stack.Screen name={"TodoList"} component={TodoList}></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer> */
    <View style={styles.container}>
      <Library></Library>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#fff",
    padding: 60
  },
});
